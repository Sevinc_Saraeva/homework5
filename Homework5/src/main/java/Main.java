public class Main {
    public static void main(String[] args) {
        Human Micael = new Human("Michael", "AAA", 1975);
        Human Laura = new Human("Laura", "AAA", 1975);
        Pet dog = new Pet("dog", "dog1");
        dog.setAge(5);
        dog.setTrickLevel(45);
        dog.setHabits(new String[]{"walk", "sleep"});
        Family family1 = new Family( Laura, Micael);
        System.out.println(family1.toString());
        Human Beck = new Human("Beck", "AAA", 2000);
        Beck.setFamily(family1);
        family1.addChild(Beck);
        System.out.println(family1);
        Human Ali = new Human("Ali", "AAA", 2002);
        family1.addChild(Ali);
        System.out.println(family1);
        family1.deleteChild(1);
        System.out.println(family1);

    }
}
